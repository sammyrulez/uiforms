from django.conf.urls.defaults import patterns, include, url
from uiforms.tmanager.views import DataDetailView ,OwnedDataView , SubmittedDataView , DeleteDataView

from django.views.generic import DetailView , ListView
from uiforms.tmanager.models import FormTemplate , TemplateField ,TemplateData 

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'uiforms.tmanager.views.home', name='home'),
    # url(r'^uiforms/', include('uiforms.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
	
	url(r'^uiform/(?P<template_id>\d+)/$','uiforms.tmanager.views.uiform',name='uiform'),
	url(r'^uiform_preview/(?P<template_id>\d+)/$','uiforms.tmanager.views.uiform_preview',name='preview'),
	url(r'^uiform_clone/(?P<template_id>\d+)/$','uiforms.tmanager.views.clone_template',name='clone'),
	url(r'^uiform_share/(?P<template_id>\d+)/$','uiforms.tmanager.views.share_template',name='share'),
	url(r'^data_owned/$',OwnedDataView.as_view(),name='owned_data'),
	url(r'^data_submitted/$',SubmittedDataView.as_view(),name='submitted_data'),
	url(r'^data_detail/(?P<pk>\d+)/$',DataDetailView.as_view(),name='detail'),
	url(r'^data_delete/(?P<pk>\d+)/$',DeleteDataView.as_view(),name='delete_data'),
	
	
	
)
