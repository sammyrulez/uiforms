from django.forms import fields , forms

def build_UIForm(template):
	fieldz = {}
	for f in template.templatefield_set.all():
		if f.filed_type == 'BOOLEAN':
			fieldz[f.label] = fields.BooleanField()
		else :
			fieldz[f.label] = fields.CharField(max_length = 100)
	form_class_name = 	"UIForm%s" % ( str(template),)
	UIForm = type(form_class_name, (forms.Form , ),fieldz)
	return UIForm
	
class EmailForm(forms.Form):
	email = fields.EmailField()
