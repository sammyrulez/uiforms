from django.contrib import admin
from models import FormTemplate, TemplateField, TemplateData, TemplateDataRow
from django.core import urlresolvers

class TemplateFieldInline(admin.TabularInline):
    model = TemplateField


class FormTemplateAdmin(admin.ModelAdmin):
    inlines = [TemplateFieldInline, ]

    def share(self, obj):
        return "<a href=\"%s\">Share</a>" % urlresolvers.reverse('share', args=[obj.id])

    share.allow_tags = True

    def preview(self, obj):
        return "<a href=\"%s\">Preview</a>" % urlresolvers.reverse('preview', args=[obj.id])

    preview.allow_tags = True


    def direct_link(self, obj):
        return "<a href=\"%s\">Direct Link</a>" % urlresolvers.reverse('uiform', args=[obj.id])

    direct_link.allow_tags = True



    list_display = ['name', 'share', 'preview','direct_link']


admin.site.register(FormTemplate, FormTemplateAdmin)

