"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from models import FormTemplate, TemplateField, TemplateData
from django.contrib.auth.models import User
import forms
from django.test.client import Client
from django.core import urlresolvers
from views import OwnedDataView


class FormTemplateTest(TestCase):
    def test_unicode(self):
        ft = FormTemplate()
        ft.name = "test"
        self.assertEquals(ft.name, str(ft))


class TemplateFieldTest(TestCase):
    def test_unicode(self):
        tf = TemplateField()
        tf.label = "test"
        self.assertEquals(tf.label, str(tf))


class TemplateDataTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def test_last_m(self):
        td = TemplateData()
        ft = FormTemplate.objects.get(pk=1)
        td.template_ref = ft
        td.submitted_by = User.objects.get(pk=2)
        td.save()
        self.assertTrue(td.modified)
        self.assertTrue(td.created)


class FormBuilderTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def test_class_creation(self):
        ft = FormTemplate.objects.get(pk=1)
        UIForm = forms.build_UIForm(ft)
        instance = UIForm()
        self.assertTrue(instance.as_p())


class CloneTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def test_clone_obj(self):
        pass #TODO


class ViewTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='admin')

    def params(self, ft):
        ok_params = {}
        for f in ft.templatefield_set.all():
            if f.filed_type == 'BOOLEAN':
                ok_params[f.label] = 'True'
            else:
                ok_params[f.label] = 1
        return ok_params
		

    def check_fieds_in_output(self, ft, content):
        for f in ft.templatefield_set.all():
            self.assertTrue(content.count(f.label) > 0)
			
    def test_home_r(self):
        response = self.client.get(urlresolvers.reverse('home'))
        self.assertEquals(302, response.status_code)


    def test_uiform(self):
        ft = FormTemplate.objects.get(pk=1)
        original_data = ft.templatedata_set.count()
        response = self.client.get(urlresolvers.reverse('uiform', args=[1]))
        self.assertEquals(200, response.status_code)
        self.check_fieds_in_output(ft, response.content)

        ok_params = self.params(ft)
        post_response = self.client.post(urlresolvers.reverse('uiform', args=[1]), ok_params)
        self.assertEquals(200, post_response.status_code)
        #check output
        self.assertTrue(post_response.content.count('Thank you') > 0)
        #check db
        ft_after = FormTemplate.objects.get(pk=1)
        self.assertTrue(ft.templatedata_set.count() > original_data)

    def test_uiform_post_previw(self):
        ft = FormTemplate.objects.get(pk=1)
        ok_params = self.params(ft)
        response = self.client.post(urlresolvers.reverse('preview', args=[1]), ok_params)
        self.assertEquals(200, response.status_code)
        self.check_fieds_in_output(ft, response.content)


    def test_uiform_get_previw(self):
        ft = FormTemplate.objects.get(pk=1)
        response = self.client.get(urlresolvers.reverse('preview', args=[1]))
        self.assertEquals(200, response.status_code)
        #check output
        self.check_fieds_in_output(ft, response.content)

    def test_clone(self):
        original_data = FormTemplate.objects.count()
        response = self.client.get(urlresolvers.reverse('clone', args=[1]))
        self.assertEquals(404, response.status_code)
        post_response = self.client.post(urlresolvers.reverse('clone', args=[1]), {'honeypot': 'dammy'})
        self.assertEquals(302, post_response.status_code) #check redirect
        self.assertTrue(FormTemplate.objects.count() > original_data) #check db

    def test_share(self):
        response = self.client.get(urlresolvers.reverse('share', args=[1]))
        self.assertEquals(200, response.status_code)
        self.assertTrue(response.content.count("email") > 0)
        #FAIL email validation
        post_response = self.client.post(urlresolvers.reverse('share', args=[1]), {'email': 'dammy'})
        self.assertEquals(200, response.status_code)
        self.assertTrue(post_response.content.count("error") > 0)

        post_response = self.client.post(urlresolvers.reverse('share', args=[1]), {'email': 'your_email@example.com'})
        self.assertEquals(302, post_response.status_code) #check redirect


class AdminTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='admin')

    def test_template_form_index(self):
        response = self.client.get(urlresolvers.reverse("admin:tmanager_formtemplate_changelist"))

        self.assertEquals(200, response.status_code)
        self.assertTrue(response.content.count("Share") > 0)
        self.assertTrue(response.content.count("Preview") > 0)

    def test_admin_index(self):
        response = self.client.get(urlresolvers.reverse("admin:index"))
        self.assertEquals(200, response.status_code)
        self.assertTrue(response.content.count("custom-module") > 0)


class ClassViewTest(TestCase):
    fixtures = ['user_fixtures.json', 'test_data_fixtures.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='admin')

    def test_detail(self):
        response = self.client.get(urlresolvers.reverse('detail', args=[1]))
        self.assertEquals(200, response.status_code)
        response = self.client.get(urlresolvers.reverse('detail', args=[2])) # not ownig
        self.assertEquals(200, response.status_code)
        response = self.client.get(urlresolvers.reverse('detail', args=[1989])) #non existing
        self.assertEquals(404, response.status_code)

    def test_owned_data(self):
        response_ow = self.client.get(urlresolvers.reverse('owned_data'))
        self.assertEquals(200, response_ow.status_code)

    #TODO Check not owned

    def test_submitted_data(self):
        response = self.client.get(urlresolvers.reverse('submitted_data'))
        self.assertEquals(200, response.status_code)

    #TODO Check not owned
	
    def test_delete_data(self):
		td = TemplateData()
		ft = FormTemplate.objects.get(pk=1)
		td.template_ref = ft
		td.submitted_by = User.objects.get(pk=2)
		td.save()
		response = self.client.post(urlresolvers.reverse('delete_data',args=[td.id]))
		self.assertEquals(302, response.status_code)
		
		
	
		
		
		
		
		
	