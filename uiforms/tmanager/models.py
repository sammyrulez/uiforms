from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.db.models import AutoField

def copy_model_instance(obj):
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and not f in obj._meta.parents.values()])
    return obj.__class__(**initial)

class FormTemplate(models.Model):
	name = models.CharField(max_length=100)
	owner = models.ForeignKey(User)
	
	def __unicode__(self):
		return self.name
		
class TemplateField(models.Model):
	label = models.CharField(max_length=100)
	filed_type = models.CharField(max_length=20, choices = ( ('BOOLEAN','Boolean') , ('INTEGER','Integer') )  )
	template = models.ForeignKey(FormTemplate)
	
	def __unicode__(self):
		return self.label
		
class TemplateData(models.Model):
	template_ref = models.ForeignKey(FormTemplate)	
	created     = models.DateTimeField(editable=False)
	modified    = models.DateTimeField(editable=False)
	submitted_by = models.ForeignKey(User)
	
	def save(self, *args, **kwargs):
		if not self.id:
			self.created = datetime.today()
		self.modified = datetime.today()
		super(TemplateData, self).save(*args, **kwargs)
	
	def __unicode__(self):
		return "%s by %s " % (str(self.template_ref) , self.submitted_by.username )
	
class TemplateDataRow(models.Model):
	template_data = models.ForeignKey(TemplateData)
	template_field = models.ForeignKey(TemplateField)
	payload = models.CharField(max_length=10)
	

	
