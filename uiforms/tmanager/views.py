from django.core.mail import send_mail
from django.template.context import RequestContext
from django.views.generic.edit import DeleteView
from models import FormTemplate, TemplateField, TemplateData, TemplateDataRow, copy_model_instance
from django.contrib.auth.models import User
import forms
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.http import Http404
import random
from django.views.generic import DetailView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core import urlresolvers


def home(request):
    return redirect('admin:index')


@login_required
def uiform(request, template_id):
    template = get_object_or_404(FormTemplate, pk=template_id)
    UIForm = forms.build_UIForm(template)
    if request.POST:
        form_instance = UIForm(request.POST)
        if form_instance.is_valid():
            data = TemplateData()
            data.template_ref = template
            data.submitted_by = request.user
            data.save()
            for f in template.templatefield_set.all():
                data_row = TemplateDataRow()
                data_row.template_data = data
                data_row.template_field = f
                data_row.payload = form_instance.cleaned_data[f.label]
                data_row.save()
            return render_to_response('admin/tmanager/thanks.html', {'form': form_instance, 'template_obj': template},
                context_instance=RequestContext(request))

    else:
        form_instance = UIForm()

    return render_to_response('admin/tmanager/uiform.html', {'form': form_instance, 'template_obj': template},
        context_instance=RequestContext(request))


def uiform_preview(request, template_id):
    template = get_object_or_404(FormTemplate, pk=template_id)
    UIForm = forms.build_UIForm(template)
    if request.POST:
        form_instance = UIForm(request.POST)
    else:
        idata = {}
        for f in template.templatefield_set.all():
            if f.filed_type == 'BOOLEAN':
                idata[f.label] = random.choice([True, False])
            else:
                idata[f.label] = random.randrange(0, 10)

        form_instance = UIForm(idata)

    return render_to_response('admin/tmanager/preview.html', {'form': form_instance, 'template_obj': template,} ,
        context_instance=RequestContext(request))


@login_required
def clone_template(request, template_id):
    if request.POST:
        template = get_object_or_404(FormTemplate, pk=template_id)
        new_template = copy_model_instance(template)
        new_template.owner = request.user
        new_template.save()
        for f in template.templatefield_set.all():
            new_f = copy_model_instance(f)
            new_f.template = new_template
            new_f.save()
        return redirect('admin:tmanager_formtemplate_changelist')
    else:
        raise Http404


@login_required
def share_template(request, template_id):
    if request.POST:
        email_form = forms.EmailForm(request.POST)
        if email_form.is_valid():
            the_message_ = "Hi \n I would like to shre with you this nice form \n %s \n Yours %s " % (urlresolvers.reverse('share', args=[template_id]),request.user.username)
            send_mail('UIForm share', the_message_, request.user.email,
                [email_form.cleaned_data['email']], fail_silently=False)

            return redirect('admin:tmanager_formtemplate_changelist')
    else:
        email_form = forms.EmailForm()

    return render_to_response('admin/tmanager/share.html', {'form': email_form},
           context_instance=RequestContext(request))


#TODO Protectedview mixin


class DataDetailView(DetailView):
    context_object_name = "template_data"
    model = TemplateData
    template_name = "admin/tmanager/data_detail.html"


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DataDetailView, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(DataDetailView, self).get_context_data(**kwargs)

        if context['template_data'].submitted_by == self.request.user:
            context['title'] = 'Submitted Data'
            context['back_url'] = urlresolvers.reverse('submitted_data')
        else:
            context['title'] = 'Owned Data'
            context['back_url'] = urlresolvers.reverse('owned_data')

        return context

    def get_object(self):
        template_data = super(DataDetailView, self).get_object()
        if template_data.submitted_by != self.request.user and template_data.template_ref.owner != self.request.user:
            raise Http404
        else:
            return template_data


class OwnedDataView(ListView):
    model = TemplateData
    context_object_name = "objects"
    template_name = "admin/tmanager/data_list.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(OwnedDataView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return TemplateData.objects.filter(template_ref__owner__id__exact=self.request.user.id)


class SubmittedDataView(ListView):
    context_object_name = "objects"
    template_name = "admin/tmanager/data_list.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SubmittedDataView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return TemplateData.objects.filter(submitted_by__id__exact=self.request.user.id)


class DeleteDataView(DeleteView):
    template_name = "admin/tmanager/confirm_delete.html"
    model = TemplateData

    def get_success_url(self):
        if self.object.submitted_by == self.request.user:
            return urlresolvers.reverse('submitted_data')
        else:
            return urlresolvers.reverse('owned_data')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DeleteDataView, self).dispatch(*args, **kwargs)
		
	
	

	
