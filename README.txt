This is my solution for the Djnago Challenge by drchrono.

Some explanations:
This is an admin App: this means that django admin has been used for data entry and basic navigation authentication. Users must be registered and have the 'staff' status
UI is minimal

The challenge didn't explains how users get to the form so I put a function to have direct links to the form for submitting data
There is a delete view that is not linked from any template. I thought it would be a nice feature but the challenge didn't contains any suggestion on the UX for deleting data

The preview function do not match exaclty the extra credit.. view function correctly handle the POST values to prototype the form, but I didn't write the UI/javascript part I will tweak it later.. maybe...